## 实现发版前拉取代码再打包，防止发版覆盖问题

### 实现效果

![实现效果](pull.png)

```
npm i -D @xccjh/git-pull
# or
yarn add -D @xccjh/git-pull
```

### 使用

- 新增pull.js
```js
const pull = require('@xccjh/git-pull');
pull(); // 👈 拉取代码
```

- package.json
```shell script
   "oss:test": "node pull.js && npm run build:test && npm run uploadOssTest && npm run uploadOssTestBak", 
```

运行`npm run oss:test`,实现构建发版前拉取代码


